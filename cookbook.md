# Galera Cluster Cookbook

## Add a new node

- Install packages:

```
# apt install --yes --no-install-recommends mariadb-server galera-3
```

- Copy configuration files from another node already member of the cluster, and update relevants parts of the configuration:

```
# systemctl stop mariadb.service
# scp db-1:/etc/mysql/mariadb.conf.d/99-galera.cnf /etc/mysql/mariadb.conf.d/
# replace 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102' 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102,192.168.181.103' -- /etc/mysql/mariadb.conf.d/99-galera.cnf
# replace 'wsrep_node_address = 192.168.181.101' 'wsrep_node_address = 192.168.181.103' -- /etc/mysql/mariadb.conf.d/99-galera.cnf
# replace 'wsrep_node_name = db-1' 'wsrep_node_name = db-3' -- /etc/mysql/mariadb.conf.d/99-galera.cnf
# systemctl start mariadb.service
```

When you start a node with the address of at least one other running node in the `wsrep_cluster_address` variable, this node automatically joins and synchronizes with the cluster.

- Check the connection state of the node:

```
# mysql -e "SHOW STATUS LIKE 'wsrep_connected';"
+-----------------+-------+
| Variable_name   | Value |
+-----------------+-------+
| wsrep_connected | ON    |
+-----------------+-------+
```

- Check the synchronization state of the node:

```
# mysql -e "SHOW STATUS LIKE 'wsrep_local_state_comment';"
+---------------------------+--------+
| Variable_name             | Value  |
+---------------------------+--------+
| wsrep_local_state_comment | Synced |
+---------------------------+--------+
```

- Update the `wsrep_cluster_address` variable in the configuration file of others nodes:

```
# replace 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102' 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102,192.168.181.103' -- /etc/mysql/mariadb.conf.d/99-galera.cnf
```

Since the runtime configuration have already been automatically updated when the new node joined, it is not needed to restart the service on these nodes.


## Remove a node

- Check that the node is up to date:

```
# mysql -e "SHOW STATUS LIKE 'wsrep_local_state_comment';"
+---------------------------+--------+
| Variable_name             | Value  |
+---------------------------+--------+
| wsrep_local_state_comment | Synced |
+---------------------------+--------+
```

- Shutdown the node:

```
# systemctl stop mariadb.service
```

- Check the state of the cluster on others nodes:

```
# mysql -e "SHOW STATUS LIKE 'wsrep_%';"
```

The value of `wsrep_cluster_size` should have decreased.

- Remove node IP from configuration

If you do not intend to rejoin it later, remove its IP from other nodes configuration file.

```
# replace 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102,192.168.181.103' 'wsrep_cluster_address = gcomm://192.168.181.101,192.168.181.102' -- /etc/mysql/mariadb.conf.d/99-galera.cnf
```

## Restart a cluster

When all nodes of a cluster have been shutdown, data are still there but the cluster is terminated. You need to bootstrap again the cluster.

- Find which node is the most advanced:

```
# galera_recovery
WSREP: Recovered position 346a3386-7799-11eb-bc67-6ba968c8bc2f:0
--wsrep_start_position=346a3386-7799-11eb-bc67-6ba968c8bc2f:0
```

- Bootstrap the most advanced node:

```
# galera_new_cluster
```

- Start all other nodes:

```
# systemctl start mariadb.service
```

- Check the state of the cluster:

```
# mysql -e "SHOW STATUS LIKE 'wsrep_cluster%'"
+--------------------------+--------------------------------------+
| Variable_name            | Value                                |
+--------------------------+--------------------------------------+
| wsrep_cluster_conf_id    | 2                                    |
| wsrep_cluster_size       | 2                                    |
| wsrep_cluster_state_uuid | 346a3386-7799-11eb-bc67-6ba968c8bc2f |
| wsrep_cluster_status     | Primary                              |
| wsrep_cluster_weight     | 2                                    |
+--------------------------+--------------------------------------+
```

## Show current configuration options

```
# mysql -e "SHOW STATUS LIKE 'wsrep%';"
# mysql -e "SHOW VARIABLES LIKE 'wsrep%';"
```

## Securing cluster

- Allow traffic to MySQL port 3306 only from application servers or load-balancer
- Allow traffic from/to Galera port 4567 only from cluster members
- Encrypt traffic between clients and servers
- Encrypt replication traffic 
- Encrypt SST traffic

## Best practices

- Writes should happen on a single node (except if application handle retry on deadlocks)
- All nodes must be close to each others (low network lantency)
- Adding to much node in a cluster impact badly the writes performances

## Resources

- <https://mariadb.com/kb/en/getting-started-with-mariadb-galera-cluster/>

# ProxySQL Cookbook

## Connect to admin console

```
# mysql -h 127.0.0.1 -P 6032 -u admin -padmin --prompt='Admin> '
```

## How configuration is loaded

ProxySQL has 3 layers of configuration :

- **runtime**: in-memory data structures of ProxySQL used by worker threads handling requests
- **memory**: in-memory database which is exposed via a MySQL-compatible interface
- **disk**: on-disk SQLite3 database, with the default location at $(DATADIR)/proxysql.db

When it starts:

- if `<datadir>/proxysql.db` does not exists, creates it from `/etc/proxysql.cnf`
- load from **disk** to **memory**
- propagate from **memory** to **runtime**

When you make a change in the admin console, it is stored in **memory**.

To make ProxySQL apply your change, you need to load it to **runtime**:

```
Admin> LOAD <item> TO RUNTIME;
```

To persist changes accross restart, you need to save it on **disk**:

```
Admin> SAVE <item> TO DISK;
```

`<item>` must be one of these values:

- MYSQL USERS
- MYSQL SERVERS
- MYSQL QUERY RULES
- MYSQL VARIABLES
- ADMIN VARIABLES
- SCHEDULER

## Add an user

```
Admin> INSERT INTO mysql_users(username, password, default_hostgroup) VALUES ('root', '', 10);
Admin> LOAD MYSQL USERS TO RUNTIME;
Admin> SAVE MYSQL USERS TO DISK;
```

## Add a node

```
Admin> INSERT INTO mysql_servers(hostgroup_id, hostname, port) VALUES (10, '192.168.181.101', 3306);
Admin> LOAD MYSQL SERVERS TO RUNTIME;
Admin> SAVE MYSQL SERVERS TO DISK;
```

## Remove a node

```
Admin> DELETE FROM mysql_servers WHERE hostname='192.168.181.103';
Admin> LOAD MYSQL SERVERS TO RUNTIME;
Admin> SAVE MYSQL SERVERS TO DISK;
```

# Create a Galera hostgroup

```
Admin> INSERT INTO mysql_galera_hostgroups (writer_hostgroup, backup_writer_hostgroup, reader_hostgroup, offline_hostgroup, active, max_writers, writer_is_also_reader, max_transactions_behind) VALUES (10, 11, 20, 30, 1, 1, 1, 100);
```

## Route requests to a specific hostgroup

```
Admin> INSERT INTO mysql_query_rules (rule_id, match_pattern, destination_hostgroup, apply) VALUES (100, '^SELECT.*', 20, 0);
Admin> INSERT INTO mysql_query_rules (rule_id, match_pattern, destination_hostgroup, apply) VALUES (200, '^SELECT.* FOR UPDATE', 10, 1);
Admin> LOAD MYSQL QUERY RULES TO RUNTIME;
Admin> SAVE MYSQL QUERY RULES TO DISK;
```

## Show the current status of the backends

```
Admin> SELECT hostgroup,srv_host,status,ConnUsed,MaxConnUsed,Queries,Latency_us FROM stats_mysql_connection_pool ORDER BY srv_host;
```

## Dump configuration file

```
Admin> SELECT CONFIG FILE;
```

## Resources

- <https://proxysql.com/documentation/>
- <https://proxysql.com/documentation/galera-configuration/>
- <https://blog.pythian.com/proxysql-configuration-file-startup-process-explained/>

# What others are doing

## GitHub

<https://github.blog/2018-06-20-mysql-high-availability-at-github/>

- MySQL Replication in a primary/replicas topology
- automatic failover with [orchestrator](https://github.com/openark/orchestrator) (no support of Galera)
- load-balancers between apps and databases servers
- each service has its own "cluster" (ie. there is not one big cluster for everything at GitHub)
- they use some other fancy stuff like service discovery and al.

## YouTube

<http://highscalability.com/youtube-architecture>

- they use [vitess](https://vitess.io) (provides automatic sharding and replication mecanisms)

## GCP recommended architectures

<https://cloud.google.com/solutions/architectures-high-availability-mysql-clusters-compute-engine?hl=fr>

## Facebook

<https://engineering.fb.com/2017/09/25/core-data/migrating-a-database-from-innodb-to-myrocks/>

<https://www.mysql.com/fr/customers/view/?id=757>

- MySQL Replication ([MyRocksDB](http://myrocks.io))

## Tumblr

<https://engineering.tumblr.com/post/151350779044/juggling-databases-between-datacenters>

- MySQL Replication and sharding with [Jetpants](https://github.com/tumblr/jetpants)

## Pinterest

<https://github.com/pinterest/mysql_utils#basics-of-mysql-at-pinterest>

> A single master with one or two slaves Historically MySQL was used with multiple writable instances in a replica set. This topology is error prone and has been simplified to a single master with one or more slaves.

# Some tools

Tools that help to manage a MySQL cluster and/or add some functionnalities.

- [kingbus](https://github.com/flike/kingbus): distributed MySQL binlog storage system to minimize replication load on the primary
- [replication-manager](https://github.com/signal18/replication-manager): high availability solution to manage MariaDB replication topologies
- [python-mysql-replication](https://github.com/noplay/python-mysql-replication): pure Python implementation of MySQL replication protocol build on top of PyMYSQL (interesting use cases)
- [go-mysql](https://github.com/siddontang/go-mysql): same as above, but in Go
- [cobar](https://github.com/alibaba/cobar): proxy for sharding databases and tables
- [jetpants](https://github.com/tumblr/jetpants): toolkit for managing billions of rows and hundreds of database machines
- [orchestrator](https://github.com/openark/orchestrator): replication topology management and HA

# NewSQL

These are relationnal databases solutions with som advantages of NoSQL.

- [Vitess](https://vitess.io)
- [RadonDB](https://radondb.io)
- [TiDB](https://pingcap.com/products/tidb/)
