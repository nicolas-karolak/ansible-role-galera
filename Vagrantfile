# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "debian/buster64"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |v|
    v.cpus = 1
    v.memory = 256
    v.check_guest_additions = false
  end

  LB_PROVISION = false
  LB_COUNT = 1
  LB_NAMES = []
  LB_IPS = []
  (1..LB_COUNT).each do |i|
    name = "lb-#{i}"
    LB_NAMES.push(name)
    ip = "192.168.181.1#{i}"
    LB_IPS.push(ip)
    config.vm.define name do |lb|
      lb.vm.hostname = name
      lb.vm.network "private_network", "ip": ip
      if i == LB_COUNT
        LB_PROVISION = true
      end
    end
  end

  DB_PROVISION = false
  DB_COUNT = 2
  DB_NAMES = []
  DB_IPS = []
  (1..DB_COUNT).each do |j|
    name = "db-#{j}"
    DB_NAMES.push(name)
    ip = "192.168.181.10#{j}"
    DB_IPS.push(ip)
    config.vm.define name do |db|
      db.vm.hostname = name
      db.vm.network "private_network", ip: ip
      if j == DB_COUNT
        DB_PROVISION = true
      end

      if LB_PROVISION and DB_PROVISION
        db.vm.provision "ansible" do |ansible|
          ansible.groups = {
            "db_server" => DB_NAMES,
            "lb_server" => LB_NAMES,
          }
          ansible.limit = "all"
          ansible.playbook = "site.yml"
          ansible.extra_vars = {
            "netdata_packages": [
              "netdata",
              "python3-mysqldb",
            ],
            "netdata_bind": "0.0.0.0",
            "mariadb_databases": [
              "dbtest",
            ],
            "mariadb_users": [
              {
                "name": "monitor",
                "password": "monitor",
                "host": "192.168.181.%",
              },
              {
                "name": "tester",
                "password": "tester",
                "host": "192.168.181.%",
                "priv": "dbtest.*:ALL",
              },
            ],
            "galera_config": [
              {
                "name": "galera",
                "priority": "99",
                "content": \
                  "[mysqld]\n" + \
                  "bind-address = 0.0.0.0\n" + \
                  "binlog_format = row\n" + \
                  "binlog_annotate_row_events\n" + \
                  "default_storage_engine = innodb\n" + \
                  "innodb_autoinc_lock_mode = 2\n" + \
                  "wsrep_on = ON\n" + \
                  "wsrep_provider = /usr/lib/galera/libgalera_smm.so\n" + \
                  "wsrep_cluster_name = galera_cluster\n" + \
                  "wsrep_cluster_address = gcomm://{{ ansible_play_hosts | map('extract', hostvars, ['ansible_eth1', 'ipv4', 'address']) | list | join(',') }}\n" + \
                  "wsrep_node_address = {{ hostvars[inventory_hostname]['ansible_eth1']['ipv4']['address'] }}\n" + \
                  "wsrep_node_name = {{ inventory_hostname }}",
              },
            ],
            "keepalived_config": \
              "global_defs {\n" + \
              "  notification_email {\n" + \
              "     sysadmin@example.com\n" + \
              "     support@example.com\n" + \
              "   }\n" + \
              "   notification_email_from {{ inventory_hostname }}@example.com\n" + \
              "   smtp_server localhost\n" + \
              "   smtp_connect_timeout 30\n" + \
              "}\n" + \
              "\n" + \
              "vrrp_instance VI_1 {\n" + \
              "    state MASTER\n" + \
              "    interface eth1\n" + \
              "    virtual_router_id 10{{ inventory_hostname.split('-')[1] }}\n" + \
              "    priority 101\n" + \
              "    advert_int 1\n" + \
              "    authentication {\n" + \
              "        auth_type PASS\n" + \
              "        auth_pass topsecret\n" + \
              "    }\n" + \
              "    virtual_ipaddress {\n" + \
              "        192.168.181.10/24\n" + \
              "    }\n" + \
              "}",
            "proxysql_mysql_variables": {
              "threads": 4,
              "max_connections": 2048,
              "default_query_delay": 0,
              "default_query_timeout": 36000000,
              "have_compress": true,
              "poll_timeout": 2000,
              "interfaces": "0.0.0.0:6033",
              "default_schema": "information_schema",
              "stacksize": 1048576,
              "server_version": "5.5.30",
              "connect_timeout_server": 3000,
              "monitor_username": "monitor",
              "monitor_password": "monitor",
              "monitor_history": 600000,
              "monitor_connect_interval": 600000,
              "monitor_ping_interval": 10000,
              "monitor_read_only_interval": 1500,
              "monitor_read_only_timeout": 500,
              "monitor_galera_healthcheck_interval": 3000,
              "ping_interval_server_msec": 120000,
              "ping_timeout_server": 500,
              "commands_stats": true,
              "sessions_sort": true,
              "connect_retries_on_failure": 10,
            },
            "proxysql_mysql_servers": [],
            "proxysql_mysql_galera_hostgroups": [
              {
                 "writer_hostgroup": 10,
                 "backup_writer_hostgroup": 11,
                 "reader_hostgroup": 20,
                 "offline_hostgroup": 30,
                 "active": 1,
                 "max_writers": 1,
                 "writer_is_also_reader": 1,
                 "max_transactions_behind": 100,
              },
            ],
            "proxysql_mysql_users": [
              {
                "username": "tester",
                "password": "tester",
                "default_hostgroup": 10,
              },
            ],
            "proxysql_mysql_query_rules": [
              {
                "rule_id": 100,
                "active": 1,
                "match_pattern": "^SELECT",
                "cache_ttl": 20000,
                "destination_hostgroup": 20,
                "apply": 0,
              },
              {
                "rule_id": 200,
                "active": 1,
                "match_pattern": "^SELECT .* FROM UPDATE$",
                "destination_hostgroup": 10,
                "apply": 1,
              },
            ],
          }
          (1..DB_COUNT).each do |j|
            ansible.extra_vars[:proxysql_mysql_servers].append({
              "address": "192.168.181.10#{j}",
              "port": 3306,
              "hostgroup": 10,
            })
          end
        end
      end

    end

  end
end
